﻿$module = Join-Path (Split-Path -parent $MyInvocation.MyCommand.Path).Replace(".tests","") -ChildPath "sitecore-automation.psd1"


Import-Module $module -Verbose -Force

Describe "credential-store" {
	BeforeEach {
		$credentialsFile = "TestDrive:\credentials.csv"
		Set-Content $credentialsFile -value "# An empty credentials store file"
	}
	AfterEach {
		Remove-Item "TestDrive:\credentials.csv"
		$script:credentialsStore = @()
	}

	Context "New-Credential" {
		It "Shall Be Created" {
			
			'TestDrive:\credentials.csv' | Should Not Contain 'SMTP-Server'

			New-StoredCredential -Key "SMTP-Server"

			$file = Join-Path $TestDrive -ChildPath 'credentials.csv'
			Export-CredentialStore -Path $file

			'TestDrive:\credentials.csv' | Should Contain 'SMTP-Server'
		}
	}
	Context "Read-Credential" {
		It "Shall Be Created" {
			
			'TestDrive:\credentials.csv' | Should Not Contain 'SMTP-Server'

			New-StoredCredential -Key "SMTP-Server"

			$file = Join-Path $TestDrive -ChildPath 'credentials.csv'
			Export-CredentialStore -Path $file

			'TestDrive:\credentials.csv' | Should Contain 'SMTP-Server'
		}
	}
	Context "One-Credential" {
		It "Should Works" {
			
			'TestDrive:\credentials.csv' | Should Not Contain 'SMTP-Server'
			'TestDrive:\credentials.csv' | Should Not Contain 'SQL-Server'

			# Create a new credentials 
			New-StoredCredential -Key "SMTP-Server" -Username "SMTP-User" -Password ("password" | ConvertTo-SecureString -AsPlainText -Force)

			$file = Join-Path $TestDrive -ChildPath 'credentials.csv'
			#Export credentials to the CSV file
			Export-CredentialStore -Path $file

			'TestDrive:\credentials.csv' | Should Contain 'SMTP-Server'
			'TestDrive:\credentials.csv' | Should Not Contain 'SQL-Server'

			# reset credential store
			$script:credentialsStore = @()

			Get-StoredCredential -Key "SMTP-Server" | Should Be $null
			Get-StoredCredential -Key "SQL-Server" | Should Be $null

			Test-StoredCredential -Key "SMTP-Server" | Should Be $false
			Test-StoredCredential -Key "SQL-Server" | Should Be $false

	

			# Read credentials from a CSV file
			Import-CredentialStore -Path $file

			$item1 = Get-StoredCredential -Key "SMTP-Server" 
			$item2 = Get-StoredCredential -Key "SQL-Server"
			$item3 = Get-StoredCredential -Key "Not-Existing-Key"

			$item1 | Should Not Be $null
			$item2 | Should Be $null
			$item3 | Should Be $null

			Test-StoredCredential -Key "SMTP-Server" | Should Be $true
			Test-StoredCredential -Key "SQL-Server" | Should Be $false

			# Encrypt data
			ConvertTo-PlainText -Secret $item1.Username | Should Be "SMTP-User"
			ConvertTo-PlainText -Secret $item1.Password | Should Be "password"
		}
	}

	Context "Full-Flow" {
		It "Should Works" {
			
			'TestDrive:\credentials.csv' | Should Not Contain 'SMTP-Server'
			'TestDrive:\credentials.csv' | Should Not Contain 'SQL-Server'

			# Create a new credentials 
			New-StoredCredential -Key "SMTP-Server" -Username "SMTP-User" -Password ("password" | ConvertTo-SecureString -AsPlainText -Force)
			New-StoredCredential -Key "SQL-Server" -Username "SQL-User" -Password ("password" | ConvertTo-SecureString -AsPlainText -Force)

			$file = Join-Path $TestDrive -ChildPath 'credentials.csv'
			#Export credentials to the CSV file
			Export-CredentialStore -Path $file

			'TestDrive:\credentials.csv' | Should Contain 'SMTP-Server'
			'TestDrive:\credentials.csv' | Should Contain 'SQL-Server'

			# reset credential store
			$script:credentialsStore = @()

			Get-StoredCredential -Key "SMTP-Server" | Should Be $null
			Get-StoredCredential -Key "SQL-Server" | Should Be $null

			# Read credentials from a CSV file
			Import-CredentialStore -Path $file

			$item1 = Get-StoredCredential -Key "SMTP-Server" 
			$item2 = Get-StoredCredential -Key "SQL-Server"
			$item3 = Get-StoredCredential -Key "Not-Existing-Key"

			$item1 | Should Not Be $null
			$item2 | Should Not Be $null
			$item3 | Should Be $null

			# Encrypt data
			ConvertTo-PlainText -Secret $item1.Username | Should Be "SMTP-User"
			ConvertTo-PlainText -Secret $item1.Password | Should Be "password"

			ConvertTo-PlainText -Secret $item2.Username | Should Be "SQL-User"
			ConvertTo-PlainText -Secret $item2.Password | Should Be "password"

		}
	}
}