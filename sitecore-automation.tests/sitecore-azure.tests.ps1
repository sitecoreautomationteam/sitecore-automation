﻿$module = Join-Path (Split-Path -parent $MyInvocation.MyCommand.Path).Replace(".tests","") -ChildPath "sitecore-automation.psd1"

$testRoot = Split-Path -Parent $MyInvocation.MyCommand.Path

Import-Module $module -Verbose -Force
Import-Module -Name Azure

Describe "sitecore-azure" {

	Context "DeployParameters" {
		It "GetFromLocalFile" {
			$azureDeployParametersPath =  "$testRoot\azuredeploy.parameters.json"
			$json = Get-LocalDeployParametersToJson -ParametersFile $azureDeployParametersPath
			$json | Should Match "deploymentId=;"
			$json | Should Match "licenseXml="
		}

		It "OutToArray" {
			$azureDeployParametersPath =  "$testRoot\azuredeploy.parameters.json"
			$json = Get-LocalDeployParametersToJson -ParametersFile $azureDeployParametersPath
			Out-AzureDeployParameters -Json $json
		}
	}
	<#
	Context "Login-CustomFile" {
		It "First-Time-PromptForCredentials" {
			
			$hostsFile = "TestDrive:\azure.csv"
			Set-Content $hostsFile -value ""

			$credentialsPath= Join-Path $TestDrive -ChildPath 'azure.csv'
			Login-AzureAccountSilent -CredentialsPath $credentialsPath -Verbose
		}

		It "Next-Time-UseStoredCredentials" {

			'TestDrive:\azure.csv' | Should  Exist
			$credentialsPath= Join-Path $TestDrive -ChildPath 'azure.csv'
			Login-AzureAccountSilent -CredentialsPath $credentialsPath -Verbose

			Remove-Item "TestDrive:\azure.csv"
		}
	}

	Context "Login-DefaultFile" {
		It "Login" {
			Login-AzureAccountSilent -Verbose
		}
	}


	Context "Region-Check" {
		It "Supported" {
			Login-AzureAccountSilent -Verbose
			Test-SitecoreAzureRegion -Location 'East US' -Verbose | Should Be $true
		}

		It "Unsupported" {
			Login-AzureAccountSilent -Verbose
			Test-SitecoreAzureRegion -Location 'East US 2' -Verbose | Should Be $false
		}
	}

	#>
	
}