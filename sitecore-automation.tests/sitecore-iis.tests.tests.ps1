﻿$module = Join-Path (Split-Path -parent $MyInvocation.MyCommand.Path).Replace(".tests","") -ChildPath "sitecore-automation.psd1"
Import-Module $module -Verbose -Force

Describe "sitecore-iis" {
	Context "Exists" {
		It "Runs" {
			$SitecorePool = @{
				AppPool = ""
				AppName = ""
				AppPath = ""
				HostName = ""
			}


			New-SitecoreAppPool -AppPoolName "Sitecore-Test-Pool"
			
			New-SitecoreWebApp -AppPool "Sitecore-Test-Pool" -HostName "sitecore-test-local4" -AppName "SitecoreApp" -AppPath "C:\Temp" -Verbose
			New-SitecoreWebApp -AppPool "Sitecore-Test-Pool" -HostName "sitecore-test-local1" -AppName "SitecoreApp" -AppPath "C:\Temp" -Verbose
			New-SitecoreWebApp -AppPool "Sitecore-Test-Pool" -HostName "sitecore-test-local2" -AppName "SitecoreApp" -AppPath "C:\Temp" -Verbose

		}
	}
}