﻿$module = Join-Path (Split-Path -parent $MyInvocation.MyCommand.Path).Replace(".tests","") -ChildPath "sitecore-automation.psd1"

Import-Module $module -Verbose -Force
# PaperCut or SMTP4Dev is necessary to tests
Describe "system-email" {
	Context "Exists" {
		It "Runs" {

			$params = Get-EmptyEmailParameters

			$params.SmtpServer = "localhost"
			$params.SmtpPort = "25"
			$params.SmtpUsername = "user"
			$params.SmtpPassword = "pasword" | ConvertTo-SecureString -AsPlainText -Force
			$params.UseSsl = $false
			$params.From = "powershell@domain.com"
			$params.To = "me@domain.com"
			$params.Subject = "Test"
			$params.Body = "Test"

			Send-Email @params -Verbose
		}

		It "Multiple-To" {

			$params = Get-EmptyEmailParameters

			$params.SmtpServer = "localhost"
			$params.SmtpPort = "25"
			$params.SmtpUsername = "user"
			$params.SmtpPassword = "pasword" | ConvertTo-SecureString -AsPlainText -Force
			$params.UseSsl = $false
			$params.From = "powershell@domain.com"
			$params.To = "me@domain.com;you@domain.com;others@domain.com"
			$params.Subject = "Multiple recipients"
			$params.Body = "This email was send to multiple recipients"

			Send-Email @params -Verbose
		}
	}


	Context "Sitecore" {
		It "Runs" {

			$params = Get-SitecoreEmailParameters

			
			$params.UseSsl = $false
			$params.From = "powershell@domain.com"
			$params.To = "me@domain.com"
			$params.Subject = "Test"
			$params.Body = "Test"

			Send-Email @params -Verbose
		}
	}
}