﻿$module = Join-Path (Split-Path -parent $MyInvocation.MyCommand.Path).Replace(".tests","") -ChildPath "sitecore-automation.psd1"

Import-Module $module -Verbose -Force

Describe "vs-utils" {
	Context "Exists" {
		It "Runs" {
			$path = 'A:\GIT\Habitat'
			Clear-CopyLocal -Path $path -Verbose -WhatIf
		}
	}
}



Describe "Backup" {
	Context "Exists" {
		It "Runs" {
			$path = 'F:\GIT\SitecorePowershellModules\sitecore-automation.tests\web.test.xml'
			Backup-File -SourceFile $path -Verbose -WhatIf
		}
	}
}