#
# sitecore_config.ps1
#
$script:SC = New-Object -TypeName PSObject

function New-InstallSetting
{
	[CmdletBinding()]
    param (
        [parameter(Mandatory=$true)]
        [string]$JsonFile
    )

	$sc= New-Object -TypeName PSObject

	$Pool = @{
				AppPool = "{Pool-Name}"
				AppName = "{Application-Name}"
				AppPath = "{Application-Path}"
				HostName = "{HostName}"
			}
	
	$sc| Add-Member -MemberType NoteProperty -Name Pool -Value $Pool
	
	$json = ConvertTo-Json -InputObject $sc

	Set-Content -Path $JsonFile -Value $json
}


function Import-InstallSetting
{
	[CmdletBinding()]
    param (
        [parameter(Mandatory=$true)]
        [string]$JsonFile
    )

	$script:SC =  (Get-Content -Raw -Path $JsonFile | ConvertFrom-Json)
}




Export-ModuleMember -Function New-InstallSetting
Export-ModuleMember -Function Import-InstallSetting