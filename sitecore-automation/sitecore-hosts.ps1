#
# sitecore_hosts.ps1
#
function Get-DefaultHosts()
{
    return Join-Path -Path $env:windir -ChildPath '\System32\drivers\etc\hosts'
}

<#
	.SYNOPSIS
	Adds host entry to hosts file.
	
	.DESCRIPTION
	
	
	.PARAMETER IpAddress
	The IP address for host entry.

	.PARAMETER HostName
	The hostname for host entry.

	.PARAMETER Comment
	The comment/description for host entry.

	.PARAMETER Path
	The path to the hosts file. The default value is '%WINDIR%\System32\drivers\etc\hosts'
	
	.EXAMPLE
		New-SitecoreHostsEntry -IpAddress 127.0.0.2 -HostName 'HostName4' -Path $hostsFile -Verbose
	
	.EXAMPLE
		New-SitecoreHostsEntry -IpAddress 127.0.0.2 -HostName 'HostName4' -Path $hostsFile -Verbose
#>
function New-SitecoreHostsEntry
{
    [CmdletBinding(SupportsShouldProcess=$true)]
    param (
        [parameter(Mandatory=$true)]
        [string]$IpAddress,
        
        [parameter(Mandatory=$true)]
        [string]$HostName,

        [parameter()]
        [string]$Comment,

        [parameter()]
        [string]$Path = (Get-DefaultHosts)        
    )
	
	Write-Verbose "Processing hosts file: '$Path'"
	Write-Verbose "Add host entry $IpAddress $HostName $Comment"
	
	$matchPattern = '^(?<IP>[0-9a-f.:]+)\s+(?<Hostname>[^\s#]+)(?<Comment>.*)$'
	$found = $false
	[System.Collections.ArrayList]$ArrayList = @()

	if( Test-Path -Path $Path )
	{
	    $content = Get-Content -Path $Path
	}
	else
	{
	    Write-Warning "File '$Path' not exists."
	    return
	}
	
	foreach( $line in $content)
	{ 
	    if( ($line.StartsWith("#")) -or ($line.Length -eq 0))
	    { 
	        $ArrayList.Add($line) | Out-Null
	        continue 
	    }
	    elseif( $line -match $matchPattern )
	    {
	        if( $Matches.Hostname -ne $null )
	        {
	            if( $Matches.Hostname -eq $HostName )
	            {
	                Write-Verbose "Host '$HostName' already exists with $($Matches.IP) $($Matches.Comment)"
	                $found = $true
	            }
	        }
	    }
	    $ArrayList.Add($line) | Out-Null
	}
	
	if( -not $found )
	{
	    if( [string]::IsNullOrWhiteSpace($Comment) )
	    {
	        $ArrayList.Add("$IPAddress`t$Hostname") | Out-Null
	    }
	    else
	    {
	        $ArrayList.Add("$IPAddress`t$Hostname`t#$Comment") | Out-Null
	    }
	
	    Set-Content -Path $Path -Value $ArrayList | Out-Null
	}
}


<#
	.SYNOPSIS
	Removes host entry from hosts file.
	
	.DESCRIPTION
	
	.PARAMETER HostName
	The hostname for host entry.


	.PARAMETER Path
	The path to the hosts file. The default value is '%WINDIR%\System32\drivers\etc\hosts'
	
	.EXAMPLE
		Remove-SitecoreHostsEntry -HostName 'HostName4'

#>
function Remove-SitecoreHostsEntry
{
    [CmdletBinding(SupportsShouldProcess=$true)]
    param (
        [parameter(Mandatory=$true)]
        [string]$HostName,

        [parameter()]
        [string]$Path = (Get-DefaultHosts)        
    )
	
	Write-Verbose "Processing hosts file: '$Path'"
	Write-Verbose "Delete host entry $HostName"
	
	$matchPattern = '^(?<IP>[0-9a-f.:]+)\s+(?<Hostname>[^\s#]+)(?<Comment>.*)$'
	$found = $false
	[System.Collections.ArrayList]$ArrayList = @()

	if( Test-Path -Path $Path )
	{
	    $content = Get-Content -Path $Path
	}
	else
	{
	    Write-Warning "File '$Path' not exists."
	    return
	}
	
	foreach( $line in $content)
	{ 
	    if( ($line.StartsWith("#")) -or ($line.Length -eq 0))
	    { 
	        $ArrayList.Add($line) | Out-Null
	        continue 
	    }
	    elseif( $line -match $matchPattern )
	    {
	        if( $Matches.Hostname -ne $null )
	        {
	            if( $Matches.Hostname -eq $HostName )
	            {
	                Write-Verbose "Host '$HostName' exists with $($Matches.IP) $($Matches.Comment)"
	                $found = $true
	            }
	        }
	    }

		if( -not $found )
	    {
			$ArrayList.Add($line) | Out-Null
		}
	}
	
	if( $found )
	{
	    Set-Content -Path $Path -Value $ArrayList | Out-Null
	}
}

Export-ModuleMember -Function New-SitecoreHostsEntry
Export-ModuleMember -Function Remove-SitecoreHostsEntry