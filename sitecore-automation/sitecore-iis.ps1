#
# sitecore_iis.ps1
#
function Install-IIS
{
	<#
	.SYNOPSIS
		Installs IIS on Windoes server
	
	.DESCRIPTION
	
	.EXAMPLE
		
	#>
	[CmdletBinding()]
    param (
        [parameter()]
        [string]$ConfigurationFilePath
    )

    if( (Test-Path -Path $ConfigurationFilePath) -eq $true )
    {
        Write-Verbose "Install IIS from file"
        Install-WindowsFeature -ConfigurationFilePath $ConfigurationFilePath
    }
    else
    {
        Write-Verbose "Install Full IIS"
        import-module servermanager
        Add-WindowsFeature web-server -includeallsubfeature -IncludeManagementTools
    }
}


function New-SitecoreAppPool 
{
	[CmdletBinding()]
    param (
        [parameter()]
        [string]$AppPoolName
    )

    Write-Verbose "Create new AppPool: $AppPoolName"

    New-WebappPool -name $AppPoolName -Force

    $pool = Get-Item IIS:\AppPools\$AppPoolName
    
    ## Configure pool for SXP requirements
    $pool.managedRuntimeVersion = "v4.0"
    $pool.processModel.loadUserProfile = $true
    $pool.processModel.maxProcesses = 1
	
    $pool | Set-Item
}


function New-SitecoreWebApp
{
	[CmdletBinding()]
    param (
        [parameter(Mandatory=$true)]
        [string]$HostName,
		[parameter(Mandatory=$true)]
        [string]$AppName,
		[parameter(Mandatory=$true)]
        [string]$AppPath,
		[parameter(Mandatory=$true)]
        [string]$AppPool
    )

    Write-Verbose "Creates a new web application named: $HostName in folder $AppPath, $AppPool"

    New-Website -Name $AppName -HostHeader $HostName -PhysicalPath $AppPath -ApplicationPool $AppPoll

	New-SitecoreHostsEntry -IpAddress '127.0.0.1' -HostName $HostName
}

Export-ModuleMember -Function Install-IIS
Export-ModuleMember -Function New-SitecoreAppPool
Export-ModuleMember -Function New-SitecoreWebApp