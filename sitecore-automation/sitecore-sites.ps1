#
# sitecore_sites.ps1
#
function Get-SitecoreSites
{
    [cmdletbinding()]
     Param(
        [parameter(ValueFromPipeline)]
        [ValidateNotNullOrEmpty()] 
        [string[]]$Path
    )

    Process
    {
        $colSites = @()
	    $files = Get-ChildItem -Path $Path -Filter *.config 
	    foreach($file in $files)
	    {	
           
		    [xml]$XmlDocument = Get-Content -Path $file.FullName 
            $site = $XmlDocument.configuration.sitecore.sites.site
           
		    if( $site -ne $null -and $site.startItem -ne $null)
            {
                 Write-Verbose "Processing file $file"
                 $colSites += $site
            } 
        }
        return $colSites;
    }
}

Export-ModuleMember -Function Get-SitecoreSites