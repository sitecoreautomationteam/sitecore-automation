#
# sitecore_users.ps1
#
<#
.Synopsis
   Displays description for Sitecore security roles.

.DESCRIPTION
   The Describe-SitecoreRights function gets all security roles assigned to the  provided identity (role or user)
   and displays role description from Sitecore doc.
   Descriptions was taken from https://doc.sitecore.net/sitecore_experience_platform/setting_up_and_maintaining/security_and_administration/users_roles_and_domains/the_security_roles

.Example
   Describe-SitecoreRights -Identity "sitecore\Robson" 

.Notes
   Written by Robert Senktas (https://twitter.com/RobsonAutomator), August 2017
#>
function Describe-SitecoreRights
{
    [CmdletBinding()]
    Param
    (
        # Identity 
        [Parameter(Mandatory=$true)]
        $Identity
    )

#region "JsonRoles"
$jsonRoles = 
@'
[
 {
   "SecurityRole": "Everyone",
   "Description": "All users are assigned the Everyone role.This role is a virtual role. It mirrors the Windows Everyone group. It does not exist in the role database but is only used for assigning and resolving security. You can use the Everyone role to assign access rights to every user or every user in a specific domain. The Everyone role is available as both a global role and a local role in every domain."
 },
 {
   "SecurityRole": "Analytics Advanced Testing",
   "Description": "Gives the user access to see additional tabs and controls in the Marketing Control Panel.You typically give this role to optimization experts, who need expanded rights when performing tests, traffic allocation, and so on."
 },
 {
   "SecurityRole": "Analytics Content Profiling",
   "Description": "Gives the user access to the content profiling functionality in the Experience Editor and in the Content Editor."
 },
 {
   "SecurityRole": "Analytics Maintaining",
   "Description": "Gives the user access to the Marketing Control Panel, the Engagement Plan Designer, and the Supervisor. The role gives the user permissions to create goal or page event messages and campaigns for messages."
 },
 {
   "SecurityRole": "Analytics Management Reporting",
   "Description": "Gives the user access to view the management reports for optimization efforts.This role is typically given to users working with optimization, who wants to view management reports for the optimization efforts. The user can still perform tests, but this is not their main objective."
 },
 {
   "SecurityRole": "Analytics Personalization",
   "Description": "Gives the user access to the personalization functionality in the Experience Editor and in the Content Editor. Members of this role can create and edit personalization rules. Users who are not members of this role can switch personalization variations."
 },
 {
   "SecurityRole": "Analytics Reporting",
   "Description": "Gives the user access to the Marketing Control Panel, the Engagement Plan Monitor, and to the Executive Dashboard."
 },
 {
   "SecurityRole": "Analytics Testing",
   "Description": "Gives the user access to the Test Lab in the Marketing Control Panel and to the test functionality in the Experience Editor and in the Content Editor.Members of this role can create and edit test variations."
 },
 {
   "SecurityRole": "Author",
   "Description": "Gives the user access to content in the content tree. This role provides access to basic item editing features, such as the Media Library and the Content Editor, with a reduced set of tabs on the ribbon.This role also has two of the Sitecore Client roles as members, so if you assign just this role to a user, the Sitecore Client Authoring and Sitecore Client Users roles will be automatically assigned to the user."
 },
 {
   "SecurityRole": "Designer",
   "Description": "Gives the user read and write access to the areas of the content tree that are required when changing layout details for individual items and groups of items via template standard values, as well as items required when configuring the Experience Editor Design Pane. This role also has two of the Sitecore Client roles assigned to it, so if you assign just this role to a user, the Sitecore Client Designing and Sitecore Client Users roles will be automatically assigned to the user. This role provides access to the Experience Editor Design Pane features and the designer options in the Content Editor.NoteThis role is not a member of the Author and Authoring roles, so it does not allow users to edit items."
 },
 {
   "SecurityRole": "Developer",
   "Description": "Gives the user access to content manipulation facilities in the Content Editor, plus all the design and authoring roles normally used by client authors and client designers. It also provides access to more functionality on the ribbon of the Content Editor to allow full development features for users assigned to this role. This role also has access to the Development Tools menu in the Sitecore menu, which gives the user access to further development tools, such as the Package Designer."
 },
 {
   "SecurityRole": "EXM Advanced Users",
   "Description": "Gives the user full access to all the functionality in the Email Experience Manager. This role is a member of the List Manager Editors role. Members of this role can:Delete a message.Change the default settings.Open or edit the engagement plan.Change the recipient lists of a subscription message.Save a message as a subscription message template.Change the target device."
 },
 {
   "SecurityRole": "EXM Users",
   "Description": "Gives the user access to all the basic functionality in the Email Experience Manager, such as create, send, and manage messages. This role is a member of the List Manager Editors role."
 },
 {
   "SecurityRole": "Experience Explorer",
   "Description": "Gives the user access to the explore mode in the Experience Editor and to manage the Presets of the Explore mode in the Marketing Control Panel. The role is intended for marketers who set up campaigns and personalization."
 },
 {
   "SecurityRole": "Facebook Message Reviewer",
   "Description": "Gives the user access to edit Facebook messages."
 },
 {
   "SecurityRole": "List Manager Editors",
   "Description": "Gives the user access to the List Manager application for marketers who need to manage contact lists. This is primarily given to EXM users. Members of this role are:Sitecore\\EXM UsersSitecore\\EXM Advanced Users"
 },
 {
   "SecurityRole": "Sitecore Client Account Managing",
   "Description": "Gives the user access to maintain users, roles, and domains in the Access Manager, the Domain Manager, the Role Manager, and the User Manager."
 },
 {
   "SecurityRole": "Sitecore Client Advanced Publishing",
   "Description": "Gives the user access to the publishing functionality in the Experience Editor and in the Content Editor.This role has access to republish in addition to the same access rights as the Sitecore Client Publishing role."
 },
 {
   "SecurityRole": "Sitecore Client Authoring",
   "Description": "Gives the user access to basic item editing features. The role is intended for client users to allow access to basic authoring features.The role only influences the Content Editor commands available, it does not influence the Desktop interface's menu or the Control Panel commands."
 },
 {
   "SecurityRole": "Sitecore Client Bucket Management",
   "Description": "Gives the user access rights to the /sitecore/content/Applications/Content Editor/Ribbons/Chunks/Item Buckets item in the Core database."
 },
 {
   "SecurityRole": "Sitecore Client Configuring",
   "Description": "Gives the user access to the Content Editor features that allow a user to change the configuration details associated with items, such as the icon associated with the item and whether the item is protected or hidden.This role adds the Configure tab to the Content Editor and displays the Appearance, Masters, and Attribute groups."
 },
 {
   "SecurityRole": "Sitecore Client Designing",
   "Description": "Gives the user access to Experience Editor Design pane features that allow a user to set layout details associated with items in the Sitecore client."
 },
 {
   "SecurityRole": "Sitecore Client Developing",
   "Description": "Gives the user access to application shortcuts and commands commonly required by developers."
 },
 {
   "SecurityRole": "Sitecore Client Forms Author",
   "Description": "Gives the user access to the minimum features of the Web Forms for Marketers module. This role allows the user to:Insert a new formEdit an existing formView the Summary report"
 },
 {
   "SecurityRole": "Sitecore Client Maintaining",
   "Description": "Gives the user access to template editing features and reporting tools. This role is intended for Sitecore super-users and developers."
 },
 {
   "SecurityRole": "Sitecore Client Publishing",
   "Description": "Gives the user access to the publishing functionality in the Experience Editor and in the Content Editor. Users that are not members of the Publishing role may still be able to publish, but only via automatic publishing features associated with Workflows.This role has access to Incremental publish, Smart publish, and Publish related items."
 },
 {
   "SecurityRole": "Sitecore Client Securing",
   "Description": "Gives the user access rights to security features in the Content Editor and other relevant applications.This role is intended for users who need to maintain users and access rights."
 },
 {
   "SecurityRole": "Sitecore Client Social Authoring",
   "Description": "Gives the user access to create messages and post them to social networks.The role is a member of the Sitecore\\Author role."
 },
 {
   "SecurityRole": "Sitecore Client Translating",
   "Description": "Gives the user access to Sitecore translation features, such as the command Scan the database for untranslated fields.The role is intended for content authors who need access to languages other than the site's default language."
 },
 {
   "SecurityRole": "Sitecore Client Users",
   "Description": "Gives the user minimal access to Sitecore. With this role, the user can log in to the Sitecore Desktop, but will not have access to any applications. All of the other Sitecore client roles are members of the Sitecore Client Users role, which means that users in any Sitecore client role are automatically members of the Sitecore Client Users role."
 },
 {
   "SecurityRole": "Sitecore Limited Content Editor",
   "Description": "Limits the amount of Content Editor functionality provided by the Sitecore Client Authoring role (which is still required for users given this role).When a content author is assigned this role, they only have access to the Home, Review, and Publish tabs on the Content Editor ribbon and have no access to copy, move, or sort from the item's right-click menu."
 },
 {
   "SecurityRole": "Sitecore Limited Page Editor",
   "Description": "Restricts the amount of functionality that is available in the Experience Editor. However, unlike the Minimal Page Editor role, users assigned this role see a simple version of the standard Experience Editor ribbon.This role limits the amount of functionality provided by the Sitecore Client Authoring role (which is still required for users given this role), but allows more functional access than the Sitecore Minimal Page Editor role."
 },
 {
   "SecurityRole": "Sitecore Local Administrators",
   "Description": "Sitecore local administrators can log in to Sitecore and manage the security applications (including assigning security) within that domain. A local administrator cannot create domains or associate domains to users.NoteThe local administrator role is a member of the Sitecore Client Users, Sitecore Client Account Managing, and Sitecore Client Securing roles. You can use this role as a shortcut to adding these roles to a user."
 },
 {
   "SecurityRole": "Sitecore Marketer Form Author",
   "Description": "This role inherits access rights from the following roles, and can be used as a shortcut to assign a user all the access rights from those roles:Sitecore Client Form AuthorAnalytics MaintainingAnalytics Reporting"
 },
 {
   "SecurityRole": "Sitecore Minimal Page Editor",
   "Description": "Restricts the amount of functionality provided in the Experience Editor to the absolute minimum and users who have been assigned this role do not have access to the Experience Editor ribbon. For example, members of the Minimal Page Editor role cannot switch personalization variations.This role limits the amount of functionality provided by the Sitecore Client Authoring role (which is still required for users given this role)."
 },
 {
   "SecurityRole": "Social Marketer Message Reviewer",
   "Description": "Gives the user access to edit social marketer messages."
 },
 {
   "SecurityRole": "Social Message Author",
   "Description": "Gives the user access to create, edit, and remove all social marketer messages."
 },
 {
   "SecurityRole": "Social Message Workflow Editor",
   "Description": "Gives the user access to use the Draft state and its commands in the Social Message workflow and the Social Marketer Message workflow."
 },
 {
   "SecurityRole": "Social Message Workflow Reviewer",
   "Description": "Gives the user access to use the Awaiting Approval state and its commands in the Social Message workflow. In addition, the user also has access to the Awaiting Post Review, Post Accepted, and Post Removed states and their commands in the Social Marketer Message workflow."
 },
 {
   "SecurityRole": "Twitter Message Reviewer",
   "Description": "Gives the user access to edit Twitter messages."
 }
]
'@

#endregion

$fullRoles = @()
if( Test-Account -Identity $identity -AccountType Role)
{
    $fullRoles = Get-Role -Identity $identity | Select-Object -ExpandProperty MemberOf | Select-Object -ExpandProperty MemberOf 
}
elseif( Test-Account -Identity $identity -AccountType User)
{
    $fullRoles = Get-User -Identity $identity  | Select-Object -ExpandProperty MemberOf | Select-Object -ExpandProperty MemberOf 
}
else
{
    Write-Warning  "User or role not exists."
	return  "User or role not exists."
}

$roles = $fullRoles |  %{ ($_.Name -split '\\')[1] } | Select-Object -Unique

$rolesDescription = ConvertFrom-Json $jsonRoles  

$descriptions = @()
foreach( $role in $roles )
{
    $descriptions  += $rolesDescription | Where-Object  { $_.SecurityRole -eq $role }
}

    return $descriptions
}


Export-ModuleMember -Function Describe-SitecoreRights