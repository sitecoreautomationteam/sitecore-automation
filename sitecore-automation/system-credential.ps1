#
# system_credential.ps1
#

# An array to store credentaials in memory (each object contains key,username,password)
$script:credentialsStore = @()

function New-StoredCredential
{
	<#
	.SYNOPSIS
		Adds a credentials to the in memory credential store.
	
	.DESCRIPTION
		A user will be asked to provide a username and password. Credentials will be stored in memory table.
		The credentails will be stored as secure string.
		You shall call Save-Credentials to save in memory table to file. 

	.EXAMPLE
		New-StoredCredential -Key 'SMTP Credentials' -Message 'Provide a credentials for SMTP server'	
	#>
	[CmdletBinding()]
    param (
        [parameter(Mandatory=$true)]
        [string]$Key,
        [parameter()]
        [string]$Username,
		[parameter()]
        [Security.SecureString]$Password = ( new-object SecureString ),
        [parameter()]
        [string]$Message="Please provide credentials:"
    )

	$credentialsItem = Get-StoredCredential($Key)
    if( $credentialsItem -ne $null )
	{
		Write-Warning "A key '$Key' already exist!"
		return
	}
	
	$user = $null
	$pwd = $null
	if( $Username -eq '' -and $Password.Length -eq 0)
	{	
		$credentials = Get-Credential -Message $Message 
		$user = $credentials.UserName | ConvertTo-SecureString -AsPlainText -Force | ConvertFrom-SecureString 
		$pwd = $credentials.Password | ConvertFrom-SecureString 
	}
	else
	{
		$user = $Username  | ConvertTo-SecureString -AsPlainText -Force | ConvertFrom-SecureString 
		$pwd =  $Password  | ConvertFrom-SecureString 
	}

	$credentialsItem = New-Object PSObject
    $credentialsItem | Add-Member �MemberType NoteProperty �Name Key �Value $Key
    $credentialsItem | Add-Member �MemberType NoteProperty �Name Username �Value $user
    $credentialsItem | Add-Member �MemberType NoteProperty �Name Password �Value $pwd

    $script:credentialsStore += $credentialsItem;
}


function Export-CredentialStore
{
	<#
	.SYNOPSIS
		Saves an in memory credentials table to the CSV file.
	
	.EXAMPLE
		Save-CredentialStore -Path 'C:\MyScriptsConfiguration'	
	#>
	[CmdletBinding()]
    param (
        [parameter(Mandatory=$true)]
        [string]$Path
    )
    $script:credentialsStore | Export-Csv -Path $Path -NoTypeInformation
}


function Import-CredentialStore
{
	<#
	.SYNOPSIS
		Loads credentials from a CSV file and stores credentials in a memory table.
	
	.EXAMPLE
		Load-CredentialStore -Path 'C:\MyScriptsConfiguration'	
	#>
	[CmdletBinding()]
    param (
        [parameter(Mandatory=$true)]
        [string]$Path
    )
	$script:credentialsStore = @()
    $script:credentialsStore += Import-Csv -Path $Path 
}

function Get-StoredCredential
{
	[CmdletBinding()]
    param (
        [parameter(Mandatory=$true)]
        [string]$Key
    )
    return $script:credentialsStore | Where-Object { $_.Key -eq $Key }
}


function Get-StoredPSCredential
{
	[CmdletBinding()]
    param (
        [parameter(Mandatory=$true)]
        [string]$Key
    )
    
	$credentials =  $script:credentialsStore | Where-Object { $_.Key -eq $Key }

	if( $credentials -ne $null)
	{
		return New-Object System.Management.Automation.PSCredential ( (ConvertTo-PlainText -Secret $credentials.Username), (ConvertTo-SecureString $credentials.Password))
	}

	return $null;
}



function Test-StoredCredential
{
	[CmdletBinding()]
    param (
        [parameter(Mandatory=$true)]
        [string]$Key
    )

    $credentialsItem = Get-StoredCredential($Key)
    if( $credentialsItem -eq $null )
    {
        return $false
    }

	return $true
}


function ConvertTo-PlainText
{
	<#
	.SYNOPSIS
		Converts a secure string to the plain text
	
	.EXAMPLE
		ConvertTo-PlainText -String 'jsjdakdajskdajsd'	
	#>
	[CmdletBinding()]
    param (
        [parameter(Mandatory=$true)]
        [string]$Secret
    )
    $secureString = $Secret | ConvertTo-SecureString 
    $BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR( $secureString )            
    return [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR)
}

Export-ModuleMember -Function ConvertTo-PlainText
Export-ModuleMember -Function New-StoredCredential
Export-ModuleMember -Function Test-StoredCredential
Export-ModuleMember -Function Get-StoredCredential
Export-ModuleMember -Function Import-CredentialStore
Export-ModuleMember -Function Export-CredentialStore
Export-ModuleMember -Function Get-StoredPSCredential

