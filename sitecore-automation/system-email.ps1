#
# system_email.ps1
#

function Get-SitecoreEmailParameters
{
	Add-Type -Assembly  Sitecore.Kernel

	$mailServer = [Sitecore.Configuration.Settings]::GetSetting("MailServer")
	$mailServerUserName = [Sitecore.Configuration.Settings]::GetSetting("MailServerUserName")
	$mailServerPassword = [Sitecore.Configuration.Settings]::GetSetting("MailServerPassword")
	$mailServerPort = [Sitecore.Configuration.Settings]::GetSetting("MailServerPort")

	return @{
		SmtpServer = $mailServer
		SmtpPort = $mailServerPort
		SmtpUsername = $mailServerUserName
		SmtpPassword = new-object SecureString($mailServerPassword)
		UseSsl = $false
		From = ""
		To = ""
		Bcc = ""
		Subject = ""
		Body = "" 	 
	}
}



function Get-EmptyEmailParameters
{
  return @{
	SmtpServer = ""
	SmtpPort = ""
	SmtpUsername = ""
	SmtpPassword = new-object SecureString
	UseSsl = $false
	From = ""
	To = ""
	Bcc = ""
	Subject = ""
	Body = "" 	 
	}
}


function Send-Email
{
	<#
	.Example
	$params = Get-EmailParameters

	$params.SmtpServer = "localhost"
	$params.From = "powershell@domain.com"
	$params.To = "me@domain.com"
	$params.Subject = "Test"
	$params.Body = "Test"

	Send-Email @params -Verbose

	#>
	[CmdletBinding()]
    param (
        [parameter(Mandatory=$true)]
        [string]$SmtpServer,
		[parameter()]
        [string]$SmtpPort = 25,
		[parameter()]
        [string]$SmtpUsername,
		[parameter()]
        [Security.SecureString]$SmtpPassword,
		[parameter()]
        [bool]$UseSsl = $false,
		[parameter(Mandatory=$true)]
        [string]$From,
		[parameter(Mandatory=$true)]
        [string]$To,
		[parameter()]
        [string]$Bcc,
		[parameter(Mandatory=$true)]
        [string]$Subject,
		[parameter(Mandatory=$true)]
        [string]$Body
    )

	Write-Verbose "Server: $SmtpServer : $SmtpPort"
	$smtp = new-object Net.Mail.SmtpClient($SmtpServer, $SmtpPort)
	$smtp.EnableSsl = $UseSsl 

	if( $SmtpUsername -ne '' -and $SmtpPassword.Length -ne 0)
	{
		$smtp.Credentials = new-object Net.NetworkCredential($SmtpUsername, $SmtpPassword)
	}

	$msg = new-object Net.Mail.MailMessage
	$msg.From = $From

	$recipients = $To.Split(";")
	foreach( $recipient in $recipients)
	{
		$msg.To.Add($recipient)
	}
	
    
    if( $Bcc -ne '' )
    {
	    $msg.Bcc.Add($Bcc)
    }

	$msg.Subject = $Subject
	$msg.Body = $Body
	$smtp.Send($msg)
}

Export-ModuleMember -Function Send-Email
Export-ModuleMember -Function Get-EmptyEmailParameters
Export-ModuleMember -function Get-SitecoreEmailParameters