#
# system_utils.ps1
#
#http://www.adminarsenal.com/admin-arsenal-blog/powershell-zip-up-files-using-net-and-add-type/
function Zip-Directory {
    Param(
      [Parameter(Mandatory=$True)][string]$DestinationFileName,
      [Parameter(Mandatory=$True)][string]$SourceDirectory,
      [Parameter(Mandatory=$False)][string]$CompressionLevel = "Optimal",
      [Parameter(Mandatory=$False)][switch]$IncludeParentDir
    )
	Write-Verbose "Compress directory $SourceDirectory to $DestinationFileName"
    Add-Type -AssemblyName System.IO.Compression.FileSystem
    $CompressionLevel    = [System.IO.Compression.CompressionLevel]::$CompressionLevel  
    [System.IO.Compression.ZipFile]::CreateFromDirectory($SourceDirectory, $DestinationFileName, $CompressionLevel, $IncludeParentDir)
}


function UnZip-Directory {
    Param(
      [Parameter(Mandatory=$True)][string]$SourceZipFile,
      [Parameter(Mandatory=$True)][string]$DestinationDirectory
    )

    Add-Type -AssemblyName System.IO.Compression.FileSystem
    
    [System.IO.Compression.ZipFile]::ExtractToDirectory($SourceZipFile, $DestinationDirectory)
}

function Backup-File {
	[CmdletBinding(SupportsShouldProcess=$true)]
	 Param(
		[Parameter(Mandatory=$True)]
		[string]$SourceFile
    )

	#More about date formating - https://technet.microsoft.com/en-us/library/ee692801.aspx
	$date = Get-Date -Format d-M-yyyy-HH-MM-ss

	$backupFile = [System.IO.Path]::GetFileNameWithoutExtension($SourceFile) + "_" +  $date + ".bak"
	$backupFolder = [System.IO.Path]::GetDirectoryName($SourceFile)

	$backupPath = Join-Path -Path $backupFolder -ChildPath $backupFile

	Copy-Item -Path $SourceFile -Destination $backupPath
}


function Set-FilesReadOnly
{
	Param(
		[Parameter(Mandatory=$true)]
		[string]$Path
	)
	Set-ItemProperty -Path $Path -Name IsReadOnly -Value $true
}