#
# vs_utils.ps1
#
function Clear-CopyLocal
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	Param(
		[Parameter(Mandatory=$true)]
		[string]$Path
	)
    
    $excludedList = @(
    'Sitecore.*',
    'Newtonsoft.*'
    )

	$solutionReferences = @();

	dir $path -Recurse *.csproj | 
	ForEach-Object {
		
		$singleFile = $_.FullName;
		Write-Verbose "Processing '$singleFile' file"

		$xml = New-Object System.Xml.XmlDocument;
		$xml.Load($singleFile);
    
		$references = $xml.Project.ItemGroup.Reference;
		$referencesList = @();
		$updateFile = $false

	    foreach($reference in $references)
	    {
		    # Choose only references to Sitecore libraries
		    if( $reference.Include -match "Sitecore.*" -or $reference.Include -match "Newtonsoft.*")
		    {
			    $referencesList += $reference;

			    # Check if <Private/> element exist
			    $node = $reference.ChildNodes | Where-Object {$_.Name -eq 'Private'}

        
			    if( $node.Count -eq 0 )
			    {
				    $private = $xml.CreateElement("Private", $xml.DocumentElement.NamespaceURI);
				    $private.InnerText = "False";
				    $reference.AppendChild($private);
					$updateFile = $true
				    Write-Verbose "`tAdd False - for $($reference.Include)."  
			    }
			    elseif( $node.'#text' -eq $true )
			    {
				    $node.'#text' = "False"
				    Write-Verbose "`tSet $($node.'#text') - for $($reference.Include)" 
					$updateFile = $true
			    }
				else
				{
					Write-Verbose "`tWas $($node.'#text') - for $($reference.Include)"
				}
		    }
	    }
		
		if ($updateFile -eq $true -and $pscmdlet.ShouldProcess($singleFile))
		{
			$xml.Save("$singleFile");
		}
	} 
}